﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practice_09_12_2019
{
    public class Account
    {
        public int Money { get; set; }
        public string Login { get; set; }
        private object locker = new object();

        public Account(string login, int money)
        {
            Money = money;
            Login = login;
        }

        public void Transaction(int sum)
        {
            lock(locker)
            {
                Console.WriteLine($"{Login}\t: {Money} => {Money + sum}");
                Money += sum;
            }
        }
    }
}
