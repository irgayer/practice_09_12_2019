﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Practice_09_12_2019
{
    class Program
    {
        private const int TRANSACTION_MIN = -300;
        private const int TRANSACTION_MAX = 300;

        static void Main(string[] args)
        {
            List<Account> accounts = new List<Account>();
            Random random = new Random();

            accounts.Add(new Account("Oleg", 1000));
            accounts.Add(new Account("Dias", 1000));
            accounts.Add(new Account("Skidan", 1000));
            accounts.Add(new Account("Yerbol", 1000));
            accounts.Add(new Account("Dinara", 1000));
            accounts.Add(new Account("Zavtra", 1000));
            accounts.Add(new Account("Examen", 1000));

            for(int i = 0; i < 1000; i++)
            {
                int randomAccount = random.Next(accounts.Count);
                int randomSum = random.Next(TRANSACTION_MIN, TRANSACTION_MAX);
                ThreadPool.QueueUserWorkItem(MakeTransaction, new object[] { accounts[randomAccount], randomSum });
            }

            Console.ReadLine();
        }

        public static void MakeTransaction(object data)
        {
            Account account = (Account)(data as object[])[0];
            int sum = (int)(data as object[])[1];

            account.Transaction(sum);
        }
    }
}
